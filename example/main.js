const OpenApiTool = require("../dist/main");
const { resolve } = require("path");

const url =
  "https://gw.alipayobjects.com/os/antfincdn/M%24jrzTTYJN/oneapi.json";

// const url = "http://localhost:8085/api/docs-json";

const outputDir = resolve(__dirname, "service");

const openApiTool = new OpenApiTool({ url });
openApiTool.generateService({
  template: "axios",
  importText: `import axios from '@/util';`,
  typescript: true,
  outputDir,
  format: (openapi) => {
    //对输出之前的内容进行格式化操作，方便适配自己的项目
    // openapi.types = openapi.types.filter((option) => {
    //   console.log(option.name);
    //   if (option.name === "ResultData") {
    //     option.diy = `
    //       export interface ResultData<T> {
    //           code: number
    //           msg: string
    //           data: T
    //       `;
    //   }
    //   return option;
    // });

    // openapi.apis.forEach((item) => {
    //   item.request.url = item.request.url.replace("/api", "");
    //   item.request.urlText = item.request.urlText.replace("/api", "");
    //   item.response.type = `ResultData<${item.response.type}>`;
    //   return item;
    // });
    return openapi;
  },
});
