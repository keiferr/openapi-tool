import axios from '@/util'; 

import type {
  CurrentUser,
  NoticeIconList,
} from './typings';



/**
 *  GET /api/currentUser
 */
export async function get_api_currentUser (
  options?: Record<string, any>,
): Promise<CurrentUser> {
  return axios({
    method: 'GET',
    url: `/api/currentUser`,
    ...(options || {}),
  })
}

/**
 *  GET /api/notices
 */
export async function get_api_notices (
  options?: Record<string, any>,
): Promise<NoticeIconList> {
  return axios({
    method: 'GET',
    url: `/api/notices`,
    ...(options || {}),
  })
}
