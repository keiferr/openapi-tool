x

  
  
  
  
  
  
  
  
  
  
  




export interface CurrentUser {
  /**  */
  name?: string;
  /**  */
  avatar?: string;
  /**  */
  userid?: string;
  /**  */
  email?: string;
  /**  */
  signature?: string;
  /**  */
  title?: string;
  /**  */
  group?: string;
  /**  */
  tags?: any[];
  /**  */
  notifyCount?: number;
  /**  */
  unreadCount?: number;
  /**  */
  country?: string;
  /**  */
  access?: string;
  /**  */
  geographic?: any;
  /**  */
  address?: string;
  /**  */
  phone?: string;
    
}


export interface LoginResult {
  /**  */
  status?: string;
  /**  */
  type?: string;
  /**  */
  currentAuthority?: string;
    
}


export interface PageParams {
  /**  */
  current?: number;
  /**  */
  pageSize?: number;
    
}


export interface RuleListItem {
  /**  */
  key?: number;
  /**  */
  disabled?: boolean;
  /**  */
  href?: string;
  /**  */
  avatar?: string;
  /**  */
  name?: string;
  /**  */
  owner?: string;
  /**  */
  desc?: string;
  /**  */
  callNo?: number;
  /**  */
  status?: number;
  /**  */
  updatedAt?: string;
  /**  */
  createdAt?: string;
  /**  */
  progress?: number;
    
}


export interface RuleList {
  /**  */
  data?: RuleListItem[];
  /** 列表的内容总数 */
  total?: number;
  /**  */
  success?: boolean;
    
}


export interface FakeCaptcha {
  /**  */
  code?: number;
  /**  */
  status?: string;
    
}


export interface LoginParams {
  /**  */
  username?: string;
  /**  */
  password?: string;
  /**  */
  autoLogin?: boolean;
  /**  */
  type?: string;
    
}


export interface ErrorResponse {
  /** 业务约定的错误码 */
  errorCode: string;
  /** 业务上的错误信息 */
  errorMessage?: string;
  /** 业务上的请求是否成功 */
  success?: boolean;
    
}


export interface NoticeIconList {
  /**  */
  data?: NoticeIconItem[];
  /** 列表的内容总数 */
  total?: number;
  /**  */
  success?: boolean;
    
}


export interface NoticeIconItemType {
    
}


export interface NoticeIconItem {
  /**  */
  id?: string;
  /**  */
  extra?: string;
  /**  */
  key?: string;
  /**  */
  read?: boolean;
  /**  */
  avatar?: string;
  /**  */
  title?: string;
  /**  */
  status?: string;
  /**  */
  datetime?: string;
  /**  */
  description?: string;
  /**  */
  type?: NoticeIconItemType;
    
}


