import axios from '@/util'; 

import type {
  RuleList,
  RuleListItem,
} from './typings';



/**
 *  GET /api/rule
 */
export async function get_api_rule (
  params: {
    /** 当前的页码 */
    current?: any;
    /** 页面的容量 */
    pageSize?: any;
  },
  options?: Record<string, any>,
): Promise<RuleList> {
  return axios({
    method: 'GET',
    url: `/api/rule`,
    params: {
      ...params,
    },
    ...(options || {}),
  })
}

/**
 *  POST /api/rule
 */
export async function post_api_rule (
  options?: Record<string, any>,
): Promise<RuleListItem> {
  return axios({
    method: 'POST',
    url: `/api/rule`,
    ...(options || {}),
  })
}

/**
 *  PUT /api/rule
 */
export async function put_api_rule (
  options?: Record<string, any>,
): Promise<RuleListItem> {
  return axios({
    method: 'PUT',
    url: `/api/rule`,
    ...(options || {}),
  })
}

/**
 *  DELETE /api/rule
 */
export async function delete_api_rule (
  options?: Record<string, any>,
): Promise<any> {
  return axios({
    method: 'DELETE',
    url: `/api/rule`,
    ...(options || {}),
  })
}
