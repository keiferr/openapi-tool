import axios from '@/util'; 

import type {
  FakeCaptcha,
  LoginResult,
} from './typings';



/**
 *  POST /api/login/captcha
 */
export async function post_api_login_captcha (
  params: {
    /** 手机号 */
    phone?: any;
  },
  options?: Record<string, any>,
): Promise<FakeCaptcha> {
  return axios({
    method: 'POST',
    url: `/api/login/captcha`,
    params: {
      ...params,
    },
    ...(options || {}),
  })
}

/**
 *  POST /api/login/outLogin
 */
export async function post_api_login_outLogin (
  options?: Record<string, any>,
): Promise<any> {
  return axios({
    method: 'POST',
    url: `/api/login/outLogin`,
    ...(options || {}),
  })
}

/**
 *  POST /api/login/account
 */
export async function post_api_login_account (
  data: {
    /**  */
    username?: string;
    /**  */
    password?: string;
    /**  */
    autoLogin?: boolean;
    /**  */
    type?: string;
  },
  options?: Record<string, any>,
): Promise<LoginResult> {
  return axios({
    method: 'POST',
    url: `/api/login/account`,
    data: {
      ...data,
    },
    ...(options || {}),
  })
}
